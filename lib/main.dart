import 'dart:math';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Column _FAIcon(Color color, String label, FaIcon icon) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        margin: const EdgeInsets.all(10),
        child: Text(label,
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.normal, color: color)),
      ),
      icon,
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Color.fromARGB(255, 255, 208, 125);
    Widget SkillSection = Container(
      padding: const EdgeInsets.all(32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'OTHER SKILLS',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 22,
                color: Color.fromARGB(255, 255, 208, 125)),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: const Text(
              'Native Programming',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _FAIcon(
                    color,
                    '1. Java',
                    FaIcon(
                      FontAwesomeIcons.java,
                      color: Colors.white,
                      size: 64,
                    )),
                _FAIcon(
                    color,
                    '2. Python',
                    FaIcon(
                      FontAwesomeIcons.python,
                      color: Colors.white,
                      size: 64,
                    )),
                _FAIcon(
                    color,
                    '3. Android',
                    FaIcon(
                      FontAwesomeIcons.android,
                      color: Colors.white,
                      size: 64,
                    )),
                _FAIcon(
                    color,
                    '4. SwiftUI',
                    FaIcon(
                      FontAwesomeIcons.swift,
                      color: Colors.white,
                      size: 64,
                    )),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: const Text(
              'Web Programing',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _FAIcon(
                    color,
                    '1. HTML',
                    FaIcon(
                      FontAwesomeIcons.html5,
                      color: Colors.white,
                      size: 64,
                    )),
                _FAIcon(
                    color,
                    '2. CSS',
                    FaIcon(
                      FontAwesomeIcons.css3Alt,
                      color: Colors.white,
                      size: 64,
                    )),
                _FAIcon(
                    color,
                    '3. Java script',
                    FaIcon(
                      FontAwesomeIcons.js,
                      color: Colors.white,
                      size: 64,
                    )),
                _FAIcon(
                    color,
                    '4. Vue Js',
                    FaIcon(
                      FontAwesomeIcons.vuejs,
                      color: Colors.white,
                      size: 64,
                    )),
              ],
            ),
          ),
        ],
      ),
    );

    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Image.asset('images/MyProfile.jpeg',
              width: 200, height: 200, fit: BoxFit.cover),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.fromLTRB(15, 0, 0, 10),
                  child: const Text(
                    'WARATCHAPON PONPIYA',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 36,
                        color: Color.fromARGB(255, 255, 208, 125)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Text(
                    'JUNIOR WEB DEVELOPER',
                    style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 18,
                        color: Color.fromARGB(255, 255, 208, 125)),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );

    Widget infoSection = Container(
      padding: const EdgeInsets.all(32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.only(left: 0),
            child: Text(
              'PERSONAL INFORMATION',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              'Date of Birth : 10 July 1999',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              'Age : 22',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              'Nationality : Thai',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              'Height : 167 CM',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              'Weight: 64 KG',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              'Marital Status : Single',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          )
        ],
      ),
    );

    Widget academicSection = Container(
      padding: const EdgeInsets.all(32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.only(left: 0),
            child: Text(
              'ACADEMIC HISTORY',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              'Burapha University',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              'bachelor degree the academic year 2021',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              'Faculty of Information Science Computer Science, GPA 3.20',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  color: Color.fromARGB(255, 255, 208, 125)),
            ),
          ),
        ],
      ),
    );

    return MaterialApp(
      title: 'MyResume',
      home: Scaffold(
          backgroundColor: Color.fromARGB(255, 0, 74, 173),
          appBar: AppBar(
            centerTitle: false,
            title: const Text('Fultter MyResume'),
          ),
          body: ListView(
            children: [
              titleSection,
              infoSection,
              SkillSection,
              academicSection
            ],
          )),
    );
  }
}
